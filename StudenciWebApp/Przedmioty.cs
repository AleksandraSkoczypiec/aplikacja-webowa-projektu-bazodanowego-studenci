//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StudenciWebApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class Przedmioty
    {
        public int ID_przedmiotu { get; set; }
        public int ID_prowadzacego { get; set; }
        public Nullable<int> ID_Specjalizacji { get; set; }
        public string Nazwa_przedmiotu { get; set; }
        public int ECTS { get; set; }
    }
}
